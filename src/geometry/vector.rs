use rand::Rng;

pub struct Vector2D {
    pub x: f64,
    pub y: f64
}

impl Vector2D {
    pub fn new_random() -> Vector2D{
        let mut range = rand::thread_rng();
        Vector2D{
            x: range.gen(),
            y: range.gen(),
        }
    }
}