use crate::geometry::vector::Vector2D;

pub struct Particle {
    pub position:   Vector2D,
    pub velocity:   Vector2D,
    pub mass:       f64,
}

impl Particle {
    pub fn compute_force_gravity(&self, acceleration_gravity: f64) -> Vector2D {
        return Vector2D{
            x: 0.0,
            y: self.mass * acceleration_gravity,
        }
    }
}